import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  FlatList,
  ActivityIndicator,
  StyleSheet,
} from 'react-native';
import RowItem from './rowItem';
import {connect} from 'react-redux';
import {getApiData, resetApiData} from '../../reduxFiles/actions';

//taking values from reducer
const mapStateToProps = (state) => {
  return {
    apiData: state.data.apiData,
    loader: state.data.loader,
  };
};

//calling action functions
const actionCreators = {
  getApiData,
  resetApiData,
};

//used React Hooks in whole project instead of class components to increase readability
const SongsList = (props) => {
  const {getApiData, resetApiData, apiData, navigation, loader} = props; //object destructuring

  const [refresh, setRefresh] = useState(false); //maintains the refresh state locally

  useEffect(() => {
    //get data from API on first page loading in DOM
    getApiData();
  }, []);

  const onRefresh = () => {
    setRefresh(true);
    getDataOnPull();
  };

  const getDataOnPull = () => {
    //reset data on pull down for refresh
    resetApiData();
    //get data again from API
    getApiData();
    //change refresh state to false
    setRefresh(false);
  };

  //till apiData is null, loader will be shown and on data load successful list will be shown

  //Row Item is created separately for better management of row view

  return (
    <View style={styles.container}>
      {!loader && loader != undefined ? (
        <FlatList
          onRefresh={() => onRefresh()}
          refreshing={refresh}
          data={apiData.results}
          renderItem={({item}) => (
            <RowItem rowItem={item} navigationProp={navigation} />
          )}
          keyExtractor={(item, index) => index.toString()}
        />
      ) : (
        <View>
          <ActivityIndicator size="large" color="blue" />
          <Text style={{color: 'blue'}}> Please Wait </Text>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default connect(mapStateToProps, actionCreators)(SongsList);
