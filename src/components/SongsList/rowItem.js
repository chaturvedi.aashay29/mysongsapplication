import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import imageLogo from '../../images/image.png';

const RowItem = (props) => {
  const {rowItem, navigationProp} = props;

  //navigate to song detail page on click
  const openDetailsPage = () => {
    navigationProp.navigate('SongDetail', {songData: rowItem});
  };

  return (
    <View
      style={{
        flexDirection: 'row',
        textAlign: 'left',
        margin: 10,
      }}>
      <Image source={imageLogo} style={{width: 70, height: 70}} />
      <TouchableOpacity
        style={{
          textAlign: 'left',
        }}
        onPress={openDetailsPage}>
        <Text
          style={{
            color: 'blue',
            fontSize: 20,
            marginHorizontal: 10,
          }}>
          {rowItem.trackName}
        </Text>
        <Text
          style={{
            color: 'blue',
            fontWeight: 'bold',
            marginLeft: 10,
          }}>
          {rowItem.artistName}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default RowItem;
