import React, {useState, useEffect} from 'react';
import {StyleSheet, Image, View} from 'react-native';
import imageLogo from '../../images/image.png';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});

export default SplashScreen = (props) => {
  const {navigation} = props;

  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('SongsList');
    }, 3000);
  }, []);

  return (
    <View style={styles.container}>
      <Image
        source={imageLogo}
        imageStyle={{resizeMode: 'stretch'}}
        style={{width: 100, height: 100}}></Image>
    </View>
  );
};
