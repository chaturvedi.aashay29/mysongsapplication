import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import imageLogo from '../../images/image.png';

const SongDetail = (props) => {
  const {songData} = props.navigation.state.params; //getting data from rowItem class for display

  return (
    <View
      style={{
        flexDirection: 'row',
        textAlign: 'left',
        margin: 10,
      }}>
      <Image source={imageLogo} style={{width: 70, height: 70}} />
      <View
        style={{
          textAlign: 'left',
        }}>
        <Text
          style={{
            color: 'blue',
            fontSize: 20,
            marginHorizontal: 10,
          }}>
          Track Name: {songData.trackName}
        </Text>
        <Text
          style={{
            color: 'blue',
            fontWeight: 'bold',
            marginLeft: 10,
          }}>
          Collection Name: {songData.collectionName}
        </Text>
        <Text
          style={{
            color: 'blue',
            fontWeight: 'bold',
            marginLeft: 10,
          }}>
          Genre: {songData.primaryGenreName}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  item_container: {
    flex: 1,
  },
  item_details: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default SongDetail;
