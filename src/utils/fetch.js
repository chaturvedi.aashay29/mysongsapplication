import axios from 'axios';
const baseURL = '';
const request = (method) => (options) => async (dispatch, getState) => {
  options.url = `${options.url}${options.url.indexOf('?') == -1 ? '?' : '&'}`;
  return axios({
    baseURL,
    method,
    ...options,
  }).catch((error) => {
    console.error('fetch error:', error);
  });
};

export const get = request('GET');
export const post = request('POST');
export const put = request('PUT');
export const patch = request('PATCH');
export const remove = request('DELETE');
