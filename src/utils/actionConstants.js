const SEND_API_DATA = 'send_data';
const RESET_API_DATA = 'reset_data_in_flatlist';

export {SEND_API_DATA, RESET_API_DATA};
