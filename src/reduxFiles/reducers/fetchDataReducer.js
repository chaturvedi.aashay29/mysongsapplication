import * as actionConstants from '../../utils/actionConstants';

const INITIAL_STATE = {
  apiData: [],
  loader: true,
};

export const FetchDataReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case actionConstants.SEND_API_DATA:
      return {
        ...state,
        apiData: action.payload,
        loader: false,
      };

    default:
      return state;
  }
};
