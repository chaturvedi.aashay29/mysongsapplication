import {combineReducers} from 'redux';
import {FetchDataReducer as data} from './fetchDataReducer';
import * as actionConstants from '../../utils/actionConstants';

const appReducers = combineReducers({
  data,
});

const rootReducer = (state, action) => {
  //reset data on pull down for refresh

  if (action.type === actionConstants.RESET_API_DATA) {
    state = undefined;
  }

  return appReducers(state, action);
};

export default rootReducer;
