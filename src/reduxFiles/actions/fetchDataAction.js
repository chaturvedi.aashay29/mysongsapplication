import {get} from '../../utils/fetch';
import {api} from '../../utils/apiConstants';
import * as actionConstants from '../../utils/actionConstants';

//get date from API
export const getApiData = () => (dispatch, getState) => {
  get({
    url: api.baseUrl,
  })(dispatch, getState)
    .then((response) => {
      if (response != undefined) {
        dispatch({
          type: actionConstants.SEND_API_DATA,
          payload: response.data,
        });
      }
    })
    .catch((error) => {
      console.log('Fetch Data error = ', error);
    });
};

//reset data fetched previously in store
export const resetApiData = () => (dispatch) => {
  dispatch({
    type: actionConstants.RESET_API_DATA,
  });
};
