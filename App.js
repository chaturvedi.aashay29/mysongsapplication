import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import SplashScreen from './src/components/SplashScreen/splashScreen';
import SongsList from './src/components/SongsList/songsList';
import SongDetail from './src/components/SongDetail/songDetail';

const MainNavigator = createStackNavigator(
  {
    SplashScreen: {screen: SplashScreen}, //Splash Screen
    SongsList: {screen: SongsList}, //List Of Songs
    SongDetail: {screen: SongDetail}, //Detail Of Song
  },
  {
    initialRouteName: 'SplashScreen', //First Screen to Open
  },
);

const App = createAppContainer(MainNavigator);

export default App;
